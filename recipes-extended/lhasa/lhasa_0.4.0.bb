# SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "lhasa is a commandline utility and library handling lha (lzh) archives."
DESCRIPTION = "Lhasa is a Free Software replacement for the Unix LHA tool, \
for decompressing .lzh (LHA / LHarc) and .lzs (LArc) archives. The backend \
for the tool is a library, so that it can be reused for other purposes."
HOMEPAGE = "https://fragglet.github.io/lhasa/"
BUGTRACKER = "https://github.com/fragglet/lhasa/issues"

LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://COPYING;md5=3610ab2b824e41d5f2d3a427fb0c212f"

PROVIDES = "lha"

SRC_URI = "git://github.com/fragglet/lhasa.git;protocol=https;branch=master"
SRCREV = "3b8f502c1480c5d9f2927e95d7e0f022b9cdd0a1"
S = "${WORKDIR}/git"

inherit autotools-brokensep pkgconfig

BBCLASSEXTEND = "native"
