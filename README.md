<!--
SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>

SPDX-License-Identifier: MIT
-->

# meta-quake

This meta-layer supplies recipes for compiling Quake from id Software with Yocto Project.

The meta-layer is has been tested on Poky using qemux86_64 with Yocto Project 4.0 "Kirkstone".

## Setup

For setting up the build and documenting how to build different configurations [kas](https://github.com/siemens/kas) is used. Please read and follow the [kas dependencies & installation](https://kas.readthedocs.io/en/latest/userguide.html#dependencies-installation) for installing kas.

## Prepare Build

Create a directory `yocto` (or any other name of your choice) and clone meta-quake into it.

```
mkdir yocto
cd yocto
git clone https://gitlab.com/toertel/meta-quake.git
```

After cloning you can switch to a different branch in case you do not want to build the default version.

```
cd meta-quake
git checkout kirkstone
cd ..
```

## Build

Build an image with Quake using X11.

```
kas build meta-quake/kas/quake-kirkstone.yaml
```

## Run

Start QEMU with the image previously built.

```
kas shell -c "runqemu core-image-base kvm slirp serial gl sdl qemuparams='-m 1024'" meta-quake/kas/quake-kirkstone.yaml
```

Login as user _root_ and and start `ezquake` inside `/usr/share/games/quake` .

```
cd /usr/share/games/quake
DISPLAY=:0 ezquake
```

**Note:** In starting of QEMU fails because _package dri was not found_ you need to install the MESA development package of your distribution. For Ubuntu `sudo apt install mesa-common-dev` does the trick.

## Known Issues

### No graphics

No output on the X display. The X terminal stays, only the title changes to *ezQuake*.

It does not make a difference wether the console or the terminal is used.

### No audio

When starting the following errors are printed.

```
ALSA lib ../../alsa-lib-1.2.6.1/src/confmisc.c:855:(parse_card) cannot find card '0'
ALSA lib ../../alsa-lib-1.2.6.1/src/conf.c:5178:(_snd_config_evaluate) function snd_func_card_inum returned error: No such file or directory
ALSA lib ../../alsa-lib-1.2.6.1/src/confmisc.c:422:(snd_func_concat) error evaluating strings
ALSA lib ../../alsa-lib-1.2.6.1/src/conf.c:5178:(_snd_config_evaluate) function snd_func_concat returned error: No such file or directory
ALSA lib ../../alsa-lib-1.2.6.1/src/confmisc.c:1334:(snd_func_refer) error evaluating name
ALSA lib ../../alsa-lib-1.2.6.1/src/conf.c:5178:(_snd_config_evaluate) function snd_func_refer returned error: No such file or directory
ALSA lib ../../alsa-lib-1.2.6.1/src/conf.c:5701:(snd_config_expand) Evaluate error: No such file or directory
ALSA lib ../../../alsa-lib-1.2.6.1/src/pcm/pcm.c:2664:(snd_pcm_open_noupdate) Unknown PCM default
```
