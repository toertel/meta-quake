# SPDX-FileCopyrightText: 2024 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Minizip Compression Library"
DESCRIPTION = "Minizip is a zip manipulation library and shipped with Zlib"
HOMEPAGE = "http://www.winimage.com/zLibDll/minizip.html"
BUGTRACKER = "https://github.com/madler/zlib/issues"

SECTION = "libs"

LICENSE = "Zlib"
LIC_FILES_CHKSUM = "file://zip.h;beginline=14;endline=30;md5=8eaa8535a3a1a2296b303f40f75385e7"

DEPENDS = "zlib"

SRC_URI = "https://zlib.net/zlib-${PV}.tar.gz"
SRC_URI[sha256sum] = "ff0ba4c292013dbc27530b3a81e1f9a813cd39de01ca5e0f8bf355702efa593e"

UPSTREAM_CHECK_URI = "http://zlib.net/"
PREMIRRORS:append = " https://zlib.net/ https://zlib.net/fossils/"

S = "${WORKDIR}/zlib-${PV}/contrib/minizip"

inherit autotools

BBCLASSEXTEND = "native nativesdk"
