# SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "The single-episode shareware version of Quake level data"
DESCRIPTION = "The single-episode shareware version of Quake is freely \
available for download from id's ftp site. It is limited to the first \
episode."

SECTION = "games"

LICENSE = "LicenseRef-Quake-Shareware-License"
LIC_FILES_CHKSUM = "\
    file://slicnse.txt;md5=5450cdeca31e270a64fc64e1b52a25a6 \
    file://licinfo.txt;md5=2b63fd0623b86b22ef915c7c0fca8ff2 \
"

DEPENDS = "lha-native"

SRC_URI = "https://ftp.gwdg.de/pub/misc/ftp.idsoftware.com/idstuff/quake/quake106.zip"
SRC_URI[md5sum] = "8cee4d03ee092909fdb6a4f84f0c1357"
SRC_URI[sha256sum] = "ec6c9d34b1ae0252ac0066045b6611a7919c2a0d78a3a66d9387a8f597553239"

inherit allarch

do_patch() {
    lha xw=${B} ${WORKDIR}/resource.1
}

do_install() {
    install -d ${D}/${datadir}/games/quake/id1
    install -m 0644 id1/pak0.pak ${D}/${datadir}/games/quake/id1/pak0.pak
}

FILES:${PN} = "${datadir}/games/quake"
