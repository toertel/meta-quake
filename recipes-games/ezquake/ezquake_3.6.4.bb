# SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Modern QuakeWorld Client"
DESCRIPTION = "Combining the features of all modern QuakeWorld® clients, \
ezQuake makes QuakeWorld® easier to start and play. The immortal first person \
shooter Quake® in the brand new skin with superb graphics and extremely fast \
gameplay."
HOMEPAGE = "https://www.ezquake.com/"
BUGTRACKER = "https://github.com/ezQuake/ezquake-source/issues"

SECTION = "games"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = " \
    file://LICENSE;md5=79c2cc4426b9e6f3b9098f02eb47aac1 \
"

DEPENDS = "virtual/libsdl2 curl jansson libjpeg-turbo libpcre2 libpng minizip speex"

SRCBRANCH = "master"
SRC_URI = " \
    gitsm://github.com/ezQuake/ezquake-source.git;protocol=https;branch=${SRCBRANCH} \
    file://use-pkg-config.patch \
"
SRCREV = "3925ba3d48cd4a03dfdaa3d29e4d79dc3b2c90b2"
S = "${WORKDIR}/git"

inherit pkgconfig

do_install() {
    install -d ${D}/${bindir}
    install -m 0755 ezquake-linux-${TARGET_ARCH} ${D}/${bindir}/ezquake

    install -d ${D}/${datadir}/icons
    install -m 0644 ezquake.ico ${D}/${datadir}/icons/ezquake.ico
}

FILES:${PN} += "${datadir}/icons/ezquake.ico"

RRECOMMENDS:${PN} = "quake-shareware-pak"
